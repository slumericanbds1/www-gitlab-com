---
layout: handbook-page-toc
title: Renewals Managers - How we do it
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the Renewals Manager handbook pages [Home](https://about.gitlab.com/handbook/customer-success/renewals-managers/Home) or [What we do](https://about.gitlab.com/handbook/customer-success/renewals-managers/What) for additional content on how renewals managers execute their tasks. 

---



# Execution: Establishing fundamental excellence 
Renewals managers (RMs) execute a series of activities across a specifically defined customer renewal lifecycle. These activities are both internal and customer facing. We seek excellence in performing the fundamental tasks of renewals management.

## Renewals lifecycle
The renewals team organizes our coverage of a customer subscriptions around particular key stages during a customers journey through the renewal process. During each stage we prescribe specific activities that encourage renewals managers to deliver distinct, stage relevant customer outcomes.


| Time Frame | Renewals Stage | Customer Outcomes (Qualitative)                                          |
|------------|----------------|----------------------------------------------------------------------------|
| Month 1    | 1. Deploy      | Customer welcomed/back, environment set-up/affirmed, startup issues identified/triaged, licenses utilized |
| Month 2    | 2. Activate    | The majority of licenses are being utilized, customer is enabled to enter the adopt stage |
| Month 3, 4, 5, 6 | 3. Adopt | Customers encouraged to optimize platform, early risk signals identified/mitigated, subscriptions show high utilization |
| Month 7    | 4. Advocate    | Customers identifying expansion opportunities, verbalizing advocacy        |
| Month 8, 9 | 5. Expand      | Customers have evaluated needs for more subs, up-tiering                     |
| Month 9, 10| 6. Prepare     | Comfortable and starting the commercial portion of the renewal lifecycle     |
| Month 11   | 7. Quoted      | Timely access to an accurate quote                                          |
| Month 12   | 8. Signing     | Signatures on quote, PO raised/sent (when necessary)                         |
| Month 12   | 9. Closed Won  | Entitled, shown gratitude, and equipped for day zero success                  |


## Opportunity Tiering
Renewals managers cover every opportunity that is in their assigned region, however, that coverage is dependent on the opportunity "tier". The tier describes the ownership of the renewal opportunity. Tiers are autopopulated for new opportunities using the tier table schema. Each Renewals Manager is responsible for collaborating with their team of Account Executives to calibrate their specific book of business with the correct tiering for each opportunity. Calibration occurs annually, and after that period, changes to tiering require manager approval. 

**Tier Table**

| Level | Opportunity revenue range | Ownership | Collaboration | Calls | Responsibilities |
| --- | --- | --- | --- | --- | --- |
| 0 | [X < 15K] | Low touch, owned by RM (PubSec has a third party motion) | N/A | N/A | N/A |
| 1 | [15K < X < 100K] | Solely owned by RM | N/A | N/A | N/A |
| 2 | [100K < X < 400K] | Collaboration w/ AE | Shared calls between AE/RM/Customer | Role specific calls only (RM’s on commercial leaning calls - RM’s own their business, they make the decision on whether to attend the calls) | Understand the status of the renewal; Track/update the risk and mitigation on the opportunities; Predictability - should be able to forecast the business |
| 3 | [X > 400K] | Solely owned by AE | N/A | N/A | Understand the status of the renewal; Track/update the risk and mitigation on the opportunities; Predictability - should be able to forecast the business |

## Renewal opportunity activities - by tier 
The following activities are recommended for each Renewals Manager as a baseline for engagement with a customer throughout their subscription journey. This framework is flexible, and ultimately every Renewals Manager must us their discretion to discern which activities make sense for their customer's experience. 

| ID | Tier 0  | Tier 1 | Tier 2 | Tier 3 | Renewals Stage | Motion | Process | Primary activities |
|---|------|------|------|------|-------------|------|------------|-------------------|
| A  | X   | X   | X   | X   | Deploy         | Internal | Package the renewal and interlock w/ account team | Prepare the renewal to be worked - contact information, partner information/cadence, MSA/procurement scenarios, custom actions) and interlock with SA/AE/CSM for details, check for active PS engagements |
| B  | X   | X   | X   |     | Deploy         | Email - Standard | Introduction  | Quick email intro previewing RM relationship, highlighting PS and other purchasable Deploy/Activate/Adoption services |
| C  |     | X   | X   |     | Deploy         | Internal | Deployment check  | Check the license utilization tool to determine if the customer has reached utilization goal |
| D  |     | X   | X   |     | Activate       | Internal | Activation check  | Check the license utilization tool to determine if the customer has reached utilization goal |
| E  | X   | X   | X   |    | Adopt | Internal | CSM/E+ RM Huddle | Ensuring the timely coordination of CSM + RM. Templated check-in (Known issues, risks, externalities at play, etc.) |
| F  | X   |     |     |     | Adopt | Email - Standard | Affirmation  | Email customer a relevant stat or story re: how other customers similar are benefitting from their GitLab subscription, and giving words of affirmation and gratitude. |
| G  | X   |     |     |     |Adopt | Email - Standard | Subscription management best practices | Push to increase utilization by suggesting subscription management best practices. Encourage the development of a buffer (<5%) to prevent over deployments. Site facts around the burden/cost/likelihood of over deployments. |
| H  | X   | X   | X   |     | Adopt | Internal | Early stage risk evaluation | Use early risk framework to identify and flag risk on the opportunity. Kick off mitigation tactics. |
| I  | X   |     |     |     | Adopt | Email - Short Form Video | Expansion warm-up | Custom touch to denote benefits of uptier, expansion. Site specifics around customers that standardize on GitLab at their organization, benefits of scale. |
| J  | X   | X   | X   | X   | Adopt | Internal | Adoption check | Check the license utilization tool to determine if the customer has reached utilization goal |
| K  | X   | X   | X   |     | Advocate | Email - Standard | Communicate subscription value consumed | Send a basic snapshot of how much subscription value the customer has consumed (tangible and intangible/unknown). Graphically appealing charts/imagery. Reminders about how to get most of subscription. |
| L  | X   | X   | X   |      | Expand | Internal | CSM + RM Huddle | Ensuring the timely coordination of CSM + RM. Templated check-in (Known issues, risks, externalities at play, etc.) |
| M  | X   | X   | X   |.   |  Expand | Internal | Late stage risk evaluation | Use late stage framework to identify and flag risk on the opportunity. Kick off mitigation tactics. |
| N  | X   | X   |     |     | Expand | External - Call | Hard expansion sell | Offer addiitional buffer, uptiers, source new opportunities for connected-new |
| O  | X   | X   | X   |     | Prepare | Email - Short form Video | Video prep customer for renewal season |
| P  |  X  |  X  |  X  |     | Prepare       | Email - Standard | Renewal transaction outreach   | Email customer requesting a meeting, informing them of approaching renewal, hyperlinking helpful documentation (business case templates, ROI templates, etc.) |
| Q  |     |  X  |  X  |     | Prepare       | Call            | Renewal meeting                 | Discuss the upcoming renewal, expansion pitch, first-step negotiations                                                        |
| R  |  X  |  X  |  X  |     | Prepare       | Internal        | Build and approve quote         | Build quote, if necessary send quote for approvals                                                                            |
| S  |     |     |     |  X  | Quoted        | Internal        | Operations Check                | Evaluation of healthy opp progression, flagging risk + changing forecast                                                      |
| T  |  X  |  X  |  X  |     | Quoted        | Email - Standard | Send quote                      | Send quote to customer (docusign or attached to an email)                                                                      |
| U  |  X  |  X  |  X  |     | Signing       | Email - Standard | Quote received notice           | Respond to customer once a signed quote has been returned                                                                       |
| V  |     |  X  |  X  |     | Signing       | Email - Standard | Recieve PO (if necessary)       | Email response that the PO has been received                                                                                   |
| W  |     |  X  |  X  |     | Signing       | Internal        | Prep and submit opp for booking | Prepare opportunity for booking, submit                                                                                       |
| X  |  X  |  X  |  X  |  X  | Closed Won    | Email           | Close email w/ gratitude, instructions | Email customer with gratitude and something unique to GitLab (moniker, closing stats - i.e. we did this in 8 touches, better than our benchmark of 10, etc.) |

# Renewals rules and policies
To build and maintain an efficient, fair, and high impact renewals organization, we have adopted the following policies and processes to describe our engagement with field sales, partners and customers. 

## Rules of engagement
  - *Growth opportunities*
    - First order business is handled by Account Executives
     - Customer add-ons are managed by Account Executives
     - Connected-new opportunities are handled by Account Executive
  - *Renewal opporutnities*
    - Customer true-ups are handled by Renewal Manager
     - Renewals Managers do not waive true-ups 
  - *Channel opporuntities* 
      -  We respect the incumbency of partners unless a lack of action by the partner puts a customers environment at risk
  - *Opportunity Coverage* 
      -  Renewals managers primary focus is managing renewal opportunities; coverage for new business and add-on's should default to the account executives team 
## Late renewals management
Our management philosophy regarding the expectations of booking renewals is simple - always timely and always accurate. 

- **Timely**: 
  - It is expected that a renewal opportunity be closed-won or closed-lost by the `subscription renewal date`.
  - We close-lost opportunities when we have full confidence that the customers will not renew (we don’t use sales automation to close opportunities) 
- **Accurate**: 
  - *No pushing*: We do not manipulate the close date to influence personal performance metrics (i.e. pushing churning opportunities into the next quarter/year)
  - *No detrimental pulling*: We do not pull opportunities forward into quarters for the purpose of personal performance metric effect if it results in compression (i.e. offering a discount for an early contract reset to improve a quarters performance)
  -  *Corresponding subscriptions*: We book opportunities that correspond, together (aka SaaS and Self-Managed migrations are booked at the same time)

# Compensation: Transparency in how we get paid
- Churn exceptions are possible, but won't be granted on the basis of influence over a book of renewal business (i.e. large churns)
- Renewal rate and NetARR components of variable compensation will occur monthly

# Metrics: A balanced performance management approach
The renewals organization puts a focus on execution through a balanced series of metrics. 

## Primary Metrics (**metric is compensated in FY24**)

- **Renewal Rate** (% of dollar)  
   `[Won ARR Basis (for Clari)] / [ARR Basis (for Clari)] `
- On-time renewal rate (% of dollar) (# of opps)  
  `Close date >= subscription renewal date`

### Secondary Metrics
- **Existing NetARR** (%)  - everything but first-order business`

### Tertiary Metrics 
- Opportunity health: Next steps completeion, stage progression, activity execution
- Under management: $ and # opps in each tier (0,1,2,3)
- Compression: Average discount
- Scaling: % of business executed through partners
- Closing Velocity: Average quote to close time (measured by stage progression)
- Customer Satisifacation: NPS, CSat scores 

I
